# Alessandria: software to manage a book library.

## Manual installation 

### Prerequisites
* Django < 4.3
* Python >= 3.9
* A database (e.g. sqlite)

### Get the sources and install alessandria

    git clone https://gitlab.com/mirelsol/alessandria.git
    cd alessandria/django-alessandria
    pip install .

* Note: if you get errors while the *pillow* module is being installed, it's probably because some libraries are missing on your system. On Debian Jessie, execute:

    apt-get install libjpeg-dev libghc-zlib-dev

### Setting up a Django project
If you don't want to use an existing django project, create one:

    django-admin startproject my_project_name


### Django app - configuration

1) In your django project, open the file *settings.py*

Add to your *INSTALLED_APPS* settings:

    INSTALLED_APPS = (
        ...
        'alessandria',
        'alessandria.templatetags.tag_extras',
        'ajax_select',
    )

2) In your django project, open the file *urls.py* of your django project

2.1) Add this import at the beginning of the file:

    from django.urls import include, re_path
    from django.contrib import admin
    from ajax_select import urls as ajax_select_urls

2.2) Add to *urls_patterns*:
  
    urlpatterns = [
        re_path(r'^admin/', admin.site.urls),
        # 'alessandria' (the 2nd parameter of the tuple passed to include) is the app_name
        re_path(r'^', include(('alessandria.urls', 'alessandria'), namespace='home')),
        re_path(r'^alessandria/', include(('alessandria.urls', 'alessandria'), namespace='alessandria')),
        re_path(r'^alessandria/ajax_lookups/', include(ajax_select_urls)),
    ]

3) Initialize the database:

    ./manage.py migrate
    ./manage.py loaddata --app alessandria ref_data

### First run

* Launch the Django server:

    ./manage.py runserver

* In your browser, go to the Django Admin page: http://127.0.0.1:8000/admin and log in as admin/admin
* Adapt configuration data to your needs (general configuration, book categories, ...)

## Usage

* In your browser go to the start page: http://127.0.0.1:8000/alessandria/

## Upgrade
* Make a backup of your previous installation (don't forget the database!)
* Get the latest version of Alessandria
* In the Django top directory, update the database running : `python manage.py migrate`
* If everything is ok, then start the server : `python manage.py v`

## Help

* If you have any questions about **the use** of Alessandria, please send a an e-mail [to this address](mailto:alessandria_users@framalistes.org). Please note that you have [to subscribe](https://framalistes.org/sympa/subscribe/alessandria_users) before.
* If you need any questions about **development** of Alessandria, please send a an e-mail [to this address](mailto:alessandria_developers@framalistes.org). Please note that you have [to subscribe](https://framalistes.org/sympa/subscribe/alessandria_developpers) before.

---

# Alessandria: application de gestion d'une bibliothèque (english version below).

## Installation manuelle

### Prérequis
* Django < 4.3
* Python >= 3.9
* Une base de données (ex. sqlite)

### Récupérer les sources et installer alessandria

    git clone https://gitlab.com/mirelsol/alessandria.git
    cd alessandria/django-alessandria
    python install .

* Remarque: si vous avez des erreurs concernant l'installation du module *pillow*, c'est probablement parcequ'il vous manque des bibliothèques système. Pour Debian Jessie, il faut exécuter:

    apt-get install libjpeg-dev libghc-zlib-dev

### Création d'un projet Django
Si vous ne souhaitez pas utiliser un projet Django existant, il faut en créer un:

    django-admin startproject nom_du_projet

### Django app - configuration
1) Dans le projet Django ouvrir le fichier *settings.py*

Ajouter dans *INSTALLED_APPS* :

    INSTALLED_APPS = (
        ...
        'alessandria',
        'alessandria.templatetags.tag_extras',
        'ajax_select',
    )

2) Dans le projet Django, ouvrir le fichier *urls.py*

2.1) Ajouter cet import en début de fichier :

    from django.urls import include, re_path
    from django.contrib import admin
    from ajax_select import urls as ajax_select_urls
     
2.2) A *urls_patterns* ajouter :

    urlpatterns = [
        re_path(r'^admin/', admin.site.urls),
        # 'alessandria' (the 2nd parameter of the tuple passed to include) is the app_name
        re_path(r'^', include(('alessandria.urls', 'alessandria'), namespace='home')),
        re_path(r'^alessandria/', include(('alessandria.urls', 'alessandria'), namespace='alessandria')),
        re_path(r'^alessandria/ajax_lookups/', include(ajax_select_urls)),
    ]

3) Initialiser la base de données :

    ./manage.py migrate
    ./manage.py loaddata --app alessandria ref_data_fr

### Premier lancement

* Dans le projet Django lancer le serveur:

    ./manage.py runserver

* Dans le navigateur aller sur la page d'administration du projet Django (http://127.0.0.1:8000/admin) et se connecter en tant que admin/admin
* Adapter la configuration de l'application à vos besoins: (configuration générale, catégorie des livres, ...)

## Utilisation
* Dans le navigateur, accéder à la page d'accueil: http://127.0.0.1:8000/alessandria/

## Mise à jour
* Faire une sauvegarde de la précédente version (ne pas oublier la base de données !)
* Télécharger la dernière version d'Alessandria
* À la racine du répertoire de Django mettre à jour la base de données en exécutant : `python manage.py migrate`
* Si tout s'est bien passé, démarrer le serveur : `python manage.py runserver`

## Aide

* Si vous avez des questions concernant l’**utilisation** d’Alessandria, envoyez un mail [à cette adresse](mailto:alessandria_users@framalistes.org). Vous devez cependant au préalable [vous abonner](https://framalistes.org/sympa/subscribe/alessandria_users).
* Si vous avez des questions concernant le **développement** d’Alessandria, envoyez un mail [à cette adresse](mailto:alessandria_developers@framalistes.org). Vous devez cependant au préalable [vous abonner](https://framalistes.org/sympa/subscribe/alessandria_developpers).
