# -*- coding: utf-8 -*-

from django.urls import path

from alessandria import views
from alessandria import ajax


app_name = "alessandria"

urlpatterns = [
    path("", views.HomeView.as_view(), name='home'),
    # /alessandria/login
    path("login", views.LoginView.as_view(), name='login'),
    path("logout", views.LogoutView.as_view(), name='logout'),
    path("reader_borrow/add/", views.ReaderBorrowCreateView.as_view(), name='reader_borrow_add'),
    path("reader_borrow/<int:pk>/", views.ReaderBorrowUpdateView.as_view(), name='reader_borrow_update'),
    path("reader_borrow/list/", views.ReaderBorrowListView.as_view(), name='reader_borrow_list'),
    path("reader_borrow/list/<str:display>/", views.ReaderBorrowListView.as_view(), name='reader_borrow_list'),
    path("reader_borrow/<int:pk>/delete/", views.ReaderBorrowDeleteView.as_view(), name='reader_borrow_delete'),
    path("reader/add/", views.ReaderCreateView.as_view(), name='reader_add'),
    path("reader/<int:pk>/", views.ReaderUpdateView.as_view(), name='reader_update'),
    path("reader/list/", views.ReaderListView.as_view(), name='reader_list'),
    path("reader/<int:pk>/delete/", views.ReaderDeleteView.as_view(), name='reader_delete'),
    path("reader/<int:pk>/disable/", views.ReaderDisableView.as_view(), name='reader_disable'),
    path("reader/<int:pk>/enable/", views.ReaderEnableView.as_view(), name='reader_enable'),

    path("author/add/", views.AuthorCreateView.as_view(), name='author_add'),
    path("author/add/<str:redirect_to_book>/", views.AuthorCreateView.as_view(), name='author_add'),
    path("author/<int:pk>/", views.AuthorUpdateView.as_view(), name='author_update'),
    path("author/<int:pk>/delete/", views.AuthorDeleteView.as_view(), name='author_delete'),
    path("author/list/", views.AuthorListView.as_view(), name='author_list'),

    path("publisher/add/", views.PublisherCreateView.as_view(), name='publisher_add'),
    path("publisher/add/<str:redirect_to_book>/", views.PublisherCreateView.as_view(), name='publisher_add'),
    
    path("publisher/<int:pk>/", views.PublisherUpdateView.as_view(), name='publisher_update'),
    path("publisher/<int:pk>/delete/", views.PublisherDeleteView.as_view(), name='publisher_delete'),
    path("publisher/list/", views.PublisherListView.as_view(), name='publisher_list'),
    path("publisher/<int:publisher_id>/books/", views.book_list_by_publisher, name='book_list_by_publisher'),

    path("book/add/", views.BookCreateView.as_view(), name='book_add'),
    path("book/add/<str:from_external_page>/", views.BookCreateView.as_view(), name='book_add'),
    path("book/<int:pk>/", views.BookUpdateView.as_view(), name='book_update'),
    path("book/<int:pk>/delete/", views.BookDeleteView.as_view(), name='book_delete'),
    path("book/list/", views.BookListView.as_view(), name='book_list'),
    path("book/no_copy_list/", views.BookNoCopyListView.as_view(), name='book_no_copy_list'),
    path("book/taken_away_list/", views.BookTakenAwayListView.as_view(), name='book_taken_away_list'),
    path("book/save_book_form_to_session/<str:dest_url>/", views.save_book_form_to_session, name='save_book_form_to_session'),
    path("book/isbn/import/", views.BookIsbnImportView.as_view(), name='book_isbn_import'),

    path("bookcopy/add/<int:book_id>/", views.BookCopyCreateView.as_view(), name='bookcopy_add'),
    path("bookcopy/<int:pk>/", views.BookCopyUpdateView.as_view(), name='bookcopy_update'),
    path("bookcopy/<int:pk>/disable/", views.BookCopyDisableView.as_view(), name='bookcopy_disable'),
    path("bookcopy/<int:pk>/delete/", views.BookCopyDeleteView.as_view(), name='bookcopy_delete'),

    path("statistics/borrowings/", views.StatisticsBorrowingsView.as_view(), name='statistics_borrowings'),
    path("statistics/readers/", views.StatisticsReadersView.as_view(), name='statistics_readers'),
    path("statistics/books_collections/", views.StatisticsBooksCollectionsView.as_view(), name='statistics_books_collections'),

    path("ajax/get/book_sub_categories/?", ajax.get_book_sub_categories, name='get_book_sub_categories'),
]
