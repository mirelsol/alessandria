FROM python:3.11
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /code/django-alessandria
RUN mkdir -p /code/django
COPY django-alessandria/ /code/django-alessandria
WORKDIR /code/django-alessandria
RUN pip install -U pip
RUN pip install .
COPY docker/web/ /code/django
WORKDIR /code/django


