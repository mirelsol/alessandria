# -*- coding: utf-8 -*-

from django.urls import include, path, re_path
from django.contrib import admin
from ajax_select import urls as ajax_select_urls

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include('alessandria.urls')),
    path("alessandria/", include('alessandria.urls', namespace='home')),
    path("alessandria/ajax_lookups/", include(ajax_select_urls)),
]
