# Alessandria: software to manage a book library.

This document describes how install and use alessandria with [docker](https://docs.docker.com/get-docker/).
For a manual installation, see `django-alessandria/README.md`.

## Prerequisites
To be able to install Alessandria you have first to install the following components:

- [docker](https://docs.docker.com/get-docker/)
- [docker compose](https://docs.docker.com/compose/)

## Installation with PostgreSQL database (default)

### Installation
To do only once:

    # Build the image
    docker compose build
    # This will ensure the db service is started before executing the next command
    docker compose up db -d
    # Create the database structure
    docker compose run --rm web_postgres python manage.py migrate --skip-checks
    # Load basic data
    docker compose run --rm web_postgres python manage.py loaddata --app alessandria ref_data_fr

### Start the container
    docker compose up -d

## Installation with sqlite3 instead of PostgreSQL (alternative)

### Installation
To do only once:

    export COMPOSE_FILE=docker-compose-sqlite.yml
    docker compose build
    docker compose run --rm web_sqlite python manage.py migrate
    docker compose run --rm web_sqlite python manage.py loaddata --app alessandria ref_data_fr
    
### Start the container
    docker compose up -d

## Use Alessandria
    http://127.0.0.1:8001 (use admin / admin to log in)
